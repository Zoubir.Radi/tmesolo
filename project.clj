(defproject tme-solo "0.1.0-SNAPSHOT"
  :description "Sujet de TME Solo"
:dependencies [[org.clojure/clojure "1.10.0"]]
  :profiles {:dev {:dependencies [[midje "1.9.6" :exclusions [org.clojure/clojure]]]
                   :plugins [[lein-midje "3.2.1"]
                             [nightlight/lein-nightlight "2.4.0"]]}
             :midje {}})
